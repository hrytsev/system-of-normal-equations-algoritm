const xs = [0.7, 0.9, 1.1, 1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1, 3.3, 3.5, 3.7, 3.9, 4.1, 4.3, 4.5, 4.7, 4.9, 5.1, 5.3, 5.5, 5.7, 5.9, 6.1, 6.3, 6.5, 6.7, 6.9, 7.1, 7.3, 7.5]
const ys = [0.3, 0.4, 0.5, 0.6, 0.7, 0.9, 1.1, 1.3, 1.6, 2, 2.4, 3, 3.6, 4.4, 5.4, 6.6, 8.1, 9.9, 12.1, 14.7, 18, 22, 26.9, 32.8, 40.1, 48.9, 59.8, 73, 89.2, 108.9, 133, 162.5, 198.5, 242.4, 296.1]

let n, ySum, xSum, xPow, xPow2, yxPow, xyPow, oneDivX, oneDivY, oneDivXPow, yDivX, xDivY, xysSum, result = [];
init();
calc();

function gauss(matrix) {
    const factor = matrix[1][0] / matrix[0][0];
    for (let i = 0; i < 3; i++) {
        matrix[1][i] -= factor * matrix[0][i];
    }
    const x = matrix[1][2] / matrix[1][1];
    const y = (matrix[0][2] - matrix[0][1] * x) / matrix[0][0];

    return { x, y };
}

function getMatrixlinear() {
    return [
        [n, xSum, ySum],
        [xSum, xPow, xysSum]
    ];
}

function getMatrixrotation() {
    return [
        [n, oneDivX, ySum],
        [oneDivX, oneDivXPow, yDivX]
    ];
}

function getMatrixhyperbola() {
    return [
        [n, xSum, oneDivY],
        [xSum, xPow, xDivY]
    ];
}

function getMatrixparabola() {
    return [
        [n, xPow, ySum],
        [xPow, xPow2, yxPow]
    ];
}

function getMatrixadvancedHyperbola() {
    return [
        [n, xSum, xDivY],
        [xSum, xPow, yxPow]
    ];
}

function init() {
    n = xs.length;
    ySum = ys.reduce((acc, value) => acc + value, 0);
    xSum = xs.reduce((acc, value) => acc + value, 0);
    xPow = xs.reduce((acc, value) => acc + value * value, 0);
    xPow2 = xs.reduce((acc, value) => acc + Math.pow(value, 4), 0);
    yxPow = ys.reduce((acc, value, index) => acc + value * Math.pow(xs[index], 2), 0);
    xyPow = xs.reduce((acc, value, index) => acc + value * Math.pow(ys[index], 2), 0);
    oneDivX = xs.reduce((acc, value) => acc + 1 / value, 0);
    oneDivY = ys.reduce((acc, value) => acc + 1 / value, 0);
    oneDivXPow = xs.reduce((acc, value) => acc + 1 / Math.pow(value, 2), 0);
    yDivX = ys.reduce((acc, value, index) => acc + value / xs[index], 0);
    xDivY = xs.reduce((acc, value, index) => acc + value / ys[index], 0);
    xysSum = xs.reduce((acc, value, index) => acc + value * ys[index], 0);
}

function calc() {
    linear();
    rotation();
    hyperbola();
    parabola();
    advancedHyperbola();
    getBest();
}

function linear() {
    let params = gauss(getMatrixlinear());
    let newY = xs.map(value => params.x + params.y * value);
    result.push({ value: getDiffAndPow(newY), function: "linear" });
}

function rotation() {
    let params = gauss(getMatrixrotation());
    let newY = xs.map(value => params.x + params.y / value);
    result.push({ value: getDiffAndPow(newY), function: "rotation" });
}

function hyperbola() {
    let params = gauss(getMatrixhyperbola());
    let newY = xs.map(value => 1 / (params.x + params.y / value));
    result.push({ value: getDiffAndPow(newY), function: "hyperbola" });
}

function parabola() {
    let params = gauss(getMatrixparabola());
    let newY = xs.map(value => params.x + params.y * Math.pow(value, 2));
    result.push({ value: getDiffAndPow(newY), function: "parabola" });
}

function advancedHyperbola() {
    let params = gauss(getMatrixadvancedHyperbola());
    let newY = xs.map(value => value / (params.x + params.y * value));
    result.push({ value: getDiffAndPow(newY), function: "advanced hyperbola" });
}

function getBest() {
    result.sort((a, b) => a.value - b.value);
    console.log(result);
}

function getDiffAndPow(newY) {
    return newY.map((value, index) => value - ys[index])
        .reduce((acc, value) => acc + Math.pow(value, 2), 0);
}
